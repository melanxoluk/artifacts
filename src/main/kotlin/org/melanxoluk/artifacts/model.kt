package org.melanxoluk.artifacts


class RequestEntry(
    val name: String,
    val content: ByteArray)

data class Version(
    val major: Int = 0,
    val minor: Int = 0,
    val patch: Int = 0,
    val suffix: String? = null
) : Comparable<Version> {
    companion object {
        fun parse(str: String): Version {
            val parts = str.split(".")
            assert(parts.size in 1..4)
            return Version(
                parts[0].toInt(),
                parts.getOrNull(1)?.toInt() ?: 0,
                parts.getOrNull(2)?.toInt() ?: 0,
                parts.getOrNull(3)
            )
        }
    }

    override fun compareTo(other: Version): Int {
        return when (val maj = major.compareTo(other.major)) {
            0 -> when (val min = minor.compareTo(other.minor)) {
                0 -> patch.compareTo(other.patch)
                else -> min
            }
            else -> maj
        }
    }

    override fun toString(): String {
        return "$major.$minor.$patch"
    }
}

enum class VersionUpdate(val update: (Version) -> Version) {
    MAJOR({ it.copy(major = it.major + 1, minor = 0, patch = 0) }),
    MINOR({ it.copy(minor = it.minor + 1, patch = 0) }),
    PATCH({ it.copy(patch = it.patch + 1) });

    operator fun invoke(version: Version) = update(version)
}

sealed class Release(open val app: String, open val version: Version, open val changeNotes: String) {
    data class SingleArtifactRelease(
        override val app: String,
        override val version: Version,
        override val changeNotes: String,
        val artifact: Artifact) : Release(app, version, changeNotes) {

        override fun copy(ch: String): Release {
            return this.copy(changeNotes = ch)
        }

        override fun artifacts(): List<Artifact> {
            return listOf(artifact)
        }
    }

    data class ManyArtifactsRelease(
        override val app: String,
        override val version: Version,
        override val changeNotes: String,
        val artifacts: List<Artifact>) : Release(app, version, changeNotes) {

        override fun copy(ch: String): Release {
            return this.copy(changeNotes = ch)
        }

        override fun artifacts(): List<Artifact> {
            return artifacts
        }
    }

    companion object {
        operator fun invoke(app: String, ver: Version, changeNotes: String, artifacts: List<Artifact>): Release {
            return if (artifacts.size == 1) {
                SingleArtifactRelease(app, ver, changeNotes, artifacts[0])
            } else ManyArtifactsRelease(app, ver, changeNotes, artifacts)
        }
    }

    abstract fun copy(ch: String): Release

    abstract fun artifacts(): List<Artifact>
}

data class Artifact(
    val filename: String,
    val link: String)
