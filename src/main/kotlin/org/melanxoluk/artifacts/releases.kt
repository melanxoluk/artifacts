package org.melanxoluk.artifacts

import java.io.File
import java.nio.file.*
import kotlin.streams.toList


class Releases(
    private val baseDirectory: String,
    private val baseUrl: String,
    private val changelogs: Changelogs) {
    private val basePath = Paths.get(baseDirectory)

    init {
        Files.createDirectories(basePath)
    }


    fun newRelease(app: String, entries: List<RequestEntry>, changeNotes: String = ""): Release {
        var path = basePath.resolve(app)
        val ver = VersionUpdate.PATCH(lastVersion(path))
        path = createDirectory(app, ver)

        val arts = saveEntries(path, app, ver, entries)
        changelogs.putChangelog(changeNotes.toByteArray(), app, ver)
        return if (arts.size == 1) Release.SingleArtifactRelease(app, ver, changeNotes, arts[0])
        else Release.ManyArtifactsRelease(app, ver, changeNotes, arts)
    }

    fun createDirectory(app: String, ver: Version): Path {
        return Files.createDirectories(basePath.resolve(Paths.get(app, ver.toString())))
    }

    fun saveEntries(dir: Path, app: String, ver: Version, entries: List<RequestEntry>): List<Artifact> {
        return entries.map {
            Files.write(dir.resolve(it.name), it.content)
            Artifact(it.name, "$baseUrl/$app/$ver/${it.name}")
        }
    }


    fun lastArtifactFile(app: String): File? {
        var path = basePath.resolve(app)
        val ver = lastVersion(path)
        path = path.resolve(ver.toString())
        if (!Files.exists(path)) return null

        return Files.list(path).toList().firstOrNull()?.toAbsolutePath()?.toFile()
    }

    fun lastArtifactFileByName(app: String, filename: String): File? {
        var path = basePath.resolve(app)
        val ver = lastVersion(path)
        path = path.resolve(ver.toString())
        if (!Files.exists(path)) return null

        return Files.list(path).toList().firstOrNull { it.fileName.toString() == filename }?.toAbsolutePath()?.toFile()
    }

    fun artifact(app: String, ver: Version, filename: String): File? {
        val path = basePath.resolve(app).resolve(ver.toString())

        if (!Files.exists(path)) return null

        return Files.list(path).toList()
            .firstOrNull { it.fileName.toString() == filename }
            ?.toAbsolutePath()?.toFile()
    }


    fun allApps(): List<String> {
        return listDirs(basePath).map { it.fileName.toString() }
    }


    fun allReleases(): List<Release> {
        return listDirs(basePath).flatMap { allReleases(it.fileName.toString()) }
    }

    fun allReleases(app: String): List<Release> {
        val path = basePath.resolve(app)
        return listDirs(path).map { release(app, it) }
    }

    fun getRelease(app: String, ver: Version): Release? {
        val path = basePath.resolve(app).resolve(ver.toString())
        return if (!Files.exists(path)) null else release(app, path)
    }

    fun updateRelease(app: String, ver: Version): Release? {
        val maxRelease = allReleases(app).maxBy { it.version }
        if (maxRelease == null) {
            return null
        }

        val newReleasePath =
            basePath
                .resolve(app)
                .resolve(ver.toString())

        Files.createDirectories(newReleasePath)

        val currReleasePath =
            basePath
                .resolve(app)
                .resolve(maxRelease.version.toString())

        Files.list(currReleasePath).forEach {
            val newPath = newReleasePath.resolve(it.fileName.toString())
            Files.write(newPath, Files.readAllBytes(it))
        }

        changelogs.putChangelog(
            changelogs.getChangelog(app, maxRelease.version).toByteArray(),
            app, ver)

        return getRelease(app, ver)
    }


    fun lastVersion(appDir: Path): Version {
        return if (Files.notExists(appDir)) Version() else {
            Files.list(appDir).toList()
                .filter { Files.isDirectory(it) }
                .map { Version.parse(it.fileName.toString()) }
                .max() ?: Version()
        }
    }

    fun release(app: String, dir: Path): Release {
        val ver = Version.parse(dir.fileName.toString())

        val artifacts = Files.list(dir)
            // release should not contains directories while
            .filter { !Files.isDirectory(it) }
            .map {
                Artifact(
                    it.fileName.toString(),
                    link(app, ver, it))
            }.toList()

        return Release(app, ver, changelogs.getChangelog(app, ver), artifacts)
    }

    fun link(app: String, ver: Version, path: Path): String {
        return "$baseUrl/$app/$ver/${path.fileName}"
    }

    fun listDirs(path: Path): List<Path> =
        if (Files.exists(path)) {
            Files.list(path)
                .filter { Files.isDirectory(it) }
                .toList()
        } else listOf()
}
