package org.melanxoluk.artifacts

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


class Changelogs(baseDirectory: String) {
    private val basePath = Paths.get(baseDirectory)

    init {
        Files.createDirectories(basePath)
    }


    fun getChangelog(app: String, ver: Version): String {
        val path = path(app, ver)
        return getContent(path)
    }

    fun getChangelogFile(app: String, ver: Version): File? {
        val path = path(app, ver)
        return if (Files.exists(path)) path.toAbsolutePath().toFile() else null
    }

    fun putChangelog(content: ByteArray, app: String, ver: Version) {
        val path = path(app, ver)
        Files.write(path, content)
    }


    private fun path(app: String, ver: Version): Path {
        val path = basePath.resolve(app)
        Files.createDirectories(path)
        return path.resolve("$ver.md")
    }

    private fun getContent(path: Path): String {
        return if (Files.exists(path)) String(Files.readAllBytes(path)) else ""
    }
}