package org.melanxoluk.artifacts.apps

import org.melanxoluk.artifacts.*


class Apps(val releases: Releases) {
    fun allApps(): List<AppInfo> {
        return releases.allReleases()
            .groupBy { it.app }
            .map { AppInfo(
                it.key,
                it.value.size,
                (it.value.lastOrNull()?.version ?: Version(0, 0, 0)).toString())
             }
    }
}