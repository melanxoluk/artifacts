package org.melanxoluk.artifacts.apps

data class AppInfo(
    val app: String,
    val releases: Int,
    val lastVersion: String)