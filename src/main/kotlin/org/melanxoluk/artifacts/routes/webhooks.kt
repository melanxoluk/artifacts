package org.melanxoluk.artifacts.routes

import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.log
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.post
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Commit
import org.gitlab4j.api.models.JobStatus
import org.gitlab4j.api.utils.JacksonJson
import org.gitlab4j.api.webhook.Event
import org.gitlab4j.api.webhook.PipelineEvent
import org.melanxoluk.artifacts.*
import org.melanxoluk.artifacts.services.GitlabReleases


fun Route.webhooks(gitlabReleases: GitlabReleases, infraService: InfraService, releases: Releases) {
    val mapper = JacksonJson().objectMapper.registerKotlinModule()

    post("/webhooks") {
        // receive success pipeline for some project
        // detect that project and request last artifact
        // put artifact in releases, notify about
        val projectId = call.request.queryParameters["id"]
        if (projectId == null) {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        val infra = infraService.getInfra()
        val project = infra.projects.getValue(projectId.toLong())
        val gitlab = infra.gitlabs.getValue(project.gitlab)
        val event = mapper.readValue(call.receiveText(), Event::class.java)
        if (event.objectKind == PipelineEvent.OBJECT_KIND) {
            val pipeline = event as PipelineEvent
            val status = pipeline.objectAttributes.status
            if (status == "success") {
                log.info("start to release project ${project.name}")

                val content = pipelineArtifact(call, gitlab, project, pipeline)
                if (content == null) {
                    call.respond(HttpStatusCode.BadRequest, "not found artifact")
                    return@post
                }

                var notes = ""
                if (pipeline.objectAttributes.ref == "master") {
                    notes = getReleaseNotes(gitlab, pipeline)
                }

                val release =
                    releases.newRelease(
                        project.name,
                        listOf(RequestEntry("artifacts.zip", content)),
                        notes)

                if (pipeline.objectAttributes.ref == "master") {
                    val gitlabRelease = gitlabReleases.makeRelease(release.app, release.version)

                    val msg = gitlabRelease?.run {
                        releaseDescription(this, project)
                    } ?: "error while creating Gitlab release for project ${release.app}"

                    project.releasesTelegram?.let {
                        val tg = infra.telegrams.getValue(it)
                        tgSendMessage(tg, msg)
                    }
                }

                project.telegram?.let {
                    val tg = infra.telegrams.getValue(it)
                    tgSendMessage(tg, releaseDescription(release, project))
                }

                log.info("${project.name} released $release")
            } else {
                log.info("callback for project ${project.name} with status $status")
            }
        }

        log.info("callback processed")
        call.respond(HttpStatusCode.OK)
    }
}

private fun pipelineArtifact(call: ApplicationCall,
                             gitlab: Gitlab,
                             project: Project,
                             event: PipelineEvent): ByteArray? {
    val gitlabProjectId = event.project.id
    val pipelineId = event.objectAttributes.id

    val api = gitlab.run { GitLabApi(host, accessToken) }
    val jobs = api.jobApi.getJobsForPipeline(gitlabProjectId, pipelineId)

    val artifactSuccessJobs = jobs.firstOrNull {
        it.status == JobStatus.SUCCESS && it.artifactsFile != null
    }

    if (artifactSuccessJobs == null) {
        call.application.log.error(
            "not found success job with artifact in pipeline $pipelineId on project ${project.name}")
        return null
    }

    return api.jobApi
        .downloadArtifactsFile(gitlabProjectId, artifactSuccessJobs.id)
        .readBytes()
}

private fun getReleaseNotes(gitlab: Gitlab, pipeline: PipelineEvent): String {
    val gitlabProjectId = pipeline.project.id

    val api = gitlab.run { GitLabApi(host, accessToken) }

    val events =
        api.eventsApi.getProjectEvents(
            gitlabProjectId,
            Constants.ActionType.PUSHED,
            null, null, null, null)

    val event = events.firstOrNull {
        it.pushData.commitTo == pipeline.objectAttributes.sha
    } ?: return ""

    val commits =
        getCommits(api,
            gitlabProjectId,
            event.pushData.commitFrom ?: event.pushData.commitTo,
            event.pushData.commitTo)

    return makeReleaseNotes(commits)
}

private fun makeReleaseNotes(commits: List<Commit>): String {
    val added = mutableListOf<String>()
    val changed = mutableListOf<String>()
    val removed = mutableListOf<String>()
    val general = mutableListOf<String>()
    val wip = mutableListOf<String>()

    for (commit in commits) {
        for (line in commit.message.split("\n")) {
            when {
                line.startsWith("+ ") -> added.add(line)
                line.startsWith("- ") -> removed.add(line)
                line.startsWith("f ") -> changed.add(line)
                line.startsWith("wip ") -> wip.add(line)
                else -> general.add(line)
            }
        }
    }

    fun StringBuilder.section(name: String, lines: List<String>) {
        if (lines.isNotEmpty()) {
            appendln(name)
            lines.forEach {
                appendln("$it ")
            }
            appendln()
        }
    }

    return buildString {
        section("**Added:**  ", added)
        section("**Changed:**  ", changed)
        section("**Removed:**  ", removed)
        section("**WIP:**  ", wip)

        if (general.size > 0) {
            appendln()
            general.forEach {
                appendln("$it  ")
            }
        }
    }
}

private fun getCommits(api: GitLabApi, project: Int, fromSha: String, toSha: String): List<Commit> {
    val commits = mutableMapOf<String, Commit>()
    var activeCommit = toSha

    while (commits[fromSha] == null) {
        val commit = api.commitsApi.getCommit(project, activeCommit)
        commits[commit.id] = commit
        val parent = commit.parentIds.firstOrNull()
        if (parent == null) {
            break
        } else {
            activeCommit = parent
        }
    }

    return commits.values.toList()
}
