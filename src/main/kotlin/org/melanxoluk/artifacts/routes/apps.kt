package org.melanxoluk.artifacts.routes

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import org.melanxoluk.artifacts.apps.Apps


fun Route.apps(apps: Apps) {
    get("/apps") {
        call.respond(apps.allApps())
    }
}