package org.melanxoluk.artifacts.routes

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondFile
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route
import org.melanxoluk.artifacts.Releases
import org.melanxoluk.artifacts.Version


fun Route.artifacts(releases: Releases) {
    route("/{app}") {
        get {
            val app = call.parameters["app"]!!

            val file = releases.lastArtifactFile(app)
            if (file != null) {
                call.response.header("Content-Disposition", "attachment; filename=\"${file.name}\"")
                call.respondFile(file)
                return@get
            }

            call.respond(HttpStatusCode.NotFound)
        }

        get("{filename}") {
            val app = call.parameters["app"]!!
            val filename = call.parameters["filename"]!!

            val file = releases.lastArtifactFileByName(app, filename)
            if (file != null) {
                call.response.header("Content-Disposition", "attachment; filename=\"${file.name}\"")
                call.respondFile(file)
                return@get
            }

            call.respond(HttpStatusCode.NotFound)
        }

        get("{ver}/{filename}") {
            val app = call.parameters["app"]!!
            val ver = Version.parse(call.parameters["ver"]!!)
            val filename = call.parameters["filename"]!!

            val file = releases.artifact(app, ver, filename)
            if (file != null) {
                call.response.header("Content-Disposition", "attachment; filename=\"${file.name}\"")
                call.respondFile(file)
                return@get
            }

            call.respond(HttpStatusCode.NotFound)
        }
    }
}
