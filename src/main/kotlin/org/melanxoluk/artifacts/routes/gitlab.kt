package org.melanxoluk.artifacts.routes

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.*
import org.melanxoluk.artifacts.Version
import org.melanxoluk.artifacts.services.GitlabReleases


fun Route.gitlabRelease(gitlabReleases: GitlabReleases) {
    route("/gitlab-release") {
        get("/{app}/{ver}") {
            val app = call.parameters["app"]!!
            val ver = Version.parse(call.parameters["ver"]!!)
            val release = gitlabReleases.makeRelease(app, ver)
            if (release == null) {
                call.respond(HttpStatusCode.BadRequest)
                return@get
            }

            call.respond(release)
        }

        get("/delete/{app}/{ver}") {
            val app = call.parameters["app"]!!
            val ver = Version.parse(call.parameters["ver"]!!)
            if (gitlabReleases.deleteRelease(app, ver)) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.BadRequest)
            }
        }
    }
}
