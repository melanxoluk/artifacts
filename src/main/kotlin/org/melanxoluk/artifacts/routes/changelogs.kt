package org.melanxoluk.artifacts.routes

import io.ktor.application.*
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.request.receiveMultipart
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.pipeline.PipelineContext
import kotlinx.io.core.readBytes
import org.melanxoluk.artifacts.*


fun Route.changelogs(changelogs: Changelogs) {
    route("/changelogs/{app}/{ver}") {
        get { downloadChangelog(changelogs) }

        get("/create") { uploadChangelog(changelogs) }
        post { uploadChangelog(changelogs) }
    }
}


suspend fun PipelineContext<Unit, ApplicationCall>.downloadChangelog(changelogs: Changelogs) {
    val app = call.parameters["app"]!!
    val ver = Version.parse(call.parameters["ver"]!!)

    val file = changelogs.getChangelogFile(app, ver)
    if (file != null) {
        call.response.header("Content-Disposition", "attachment; filename=\"${file.name}\"")
        call.respondFile(file)
        return
    }

    call.respond(HttpStatusCode.NotFound)
}

suspend fun PipelineContext<Unit, ApplicationCall>.uploadChangelog(changelogs: Changelogs) {
    val app = call.parameters["app"]!!
    val ver = Version.parse(call.parameters["ver"]!!)

    val entry = call.receiveMultipart().readAllParts().map { part ->
        when (part) {
            is PartData.FileItem -> {
                application.log.info("file received ${part.originalFileName}")
                RequestEntry(part.originalFileName ?: "unknown", part.provider().readBytes())
            }

            else -> {
                call.respond(HttpStatusCode.BadRequest, "Not File")
                return
            }
        }
    }.first()

    changelogs.putChangelog(entry.content, app, ver)

    call.respond(HttpStatusCode.OK)
}
