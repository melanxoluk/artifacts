package org.melanxoluk.artifacts.routes

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.*
import kotlinx.io.core.readBytes
import org.melanxoluk.artifacts.*


fun Route.releases(releases: Releases) {
    route("/releases") {
        post("/{app}") {
            log.info("new release income: " + call.parameters["app"])

            val entries = call.receiveMultipart().readAllParts().map { part ->
                when (part) {
                    is PartData.FileItem -> {
                        log.info("file received ${part.originalFileName}")
                        RequestEntry(part.originalFileName ?: "unknown", part.provider().readBytes())
                    }

                    else -> {
                        call.respond(HttpStatusCode.BadRequest, "Not File")
                        return@post
                    }
                }
            }

            if (entries.isEmpty()) {
                call.respond(HttpStatusCode.BadRequest, "No Files")
                return@post
            }

            // save files
            call.respond(
                releases.newRelease(
                    call.parameters["app"]!!,
                    entries
                )
            )
        }

        // todo make new release
        /*post("/{type}/{app}") {
            val app = call.parameters["app"]
        }*/

        // list releases
        get {
            call.respond(releases.allReleases())
        }

        get("/{app}") {
            val app = call.parameters["app"]!!
            call.respond(releases.allReleases(app).sortedByDescending { it.version })
        }

        get("/{app}/{ver}") {
            val app = call.parameters["app"]!!
            val ver = Version.parse(call.parameters["ver"]!!)
            releases.getRelease(app, ver)?.let {
                call.respond(it)
                return@get
            }

            call.respond(HttpStatusCode.BadRequest)
        }

        get("/update/{app}/{ver}") {
            val app = call.parameters["app"]!!
            val ver = Version.parse(call.parameters["ver"]!!)
            call.respond(releases.updateRelease(app, ver) ?: "{}")
        }
    }
}

