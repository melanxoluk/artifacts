package org.melanxoluk.artifacts

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import io.ktor.application.ApplicationCall
import io.ktor.application.application
import io.ktor.application.log
import io.ktor.util.pipeline.PipelineContext


fun tgSendMessage(telegram: Telegram, msg: String) = with(telegram) {
    TelegramBot(token).execute(
        SendMessage(channel, msg)
            .parseMode(ParseMode.Markdown)
            .disableWebPagePreview(true))
}

fun releaseDescription(release: Release, project: Project): String =
    """
    |NEW RELEASE 
    |
    |**Project:** ${project.name}
    |**Version:** ${release.version}
    | 
    |${release.changeNotes}
    """.trimIndent().trimMargin()

val PipelineContext<Unit, ApplicationCall>.log get() = application.log
