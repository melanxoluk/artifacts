package org.melanxoluk.artifacts

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get


class InfraService(private val auth: String) {
    private val host = "https://infra.mln.pw"

    private val client = HttpClient(OkHttp) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }

    private var infra: Infra? = null

    suspend fun getInfra() = if (infra == null) {
        infra = downloadInfra()
        infra!!
    } else infra!!


    private suspend fun downloadInfra(): Infra {
        return client.get("$host/infra?auth=$auth")
    }

    suspend fun reload(): Infra {
        infra = downloadInfra()
        return infra!!
    }
}


data class Infra(
    val gitlabs: Map<Long, Gitlab>,
    val telegrams: Map<Long, Telegram>,
    val projects: Map<Long, Project>) {

    fun findProject(app: String): Project? {
        return projects.values.firstOrNull { it.name == app }
    }

    fun findGitlab(app: String): Gitlab? {
        return gitlabs.values.firstOrNull { it.name == app }
    }
}

data class Gitlab(
    val id: Long,
    val name: String,
    val host: String,
    val accessToken: String,
    val username: String?,
    val password: String?)

data class Telegram(
    val id: Long,
    val name: String,
    val token: String,
    val channel: String)

data class Project(
    val id: Long,
    val name: String,
    val gitlab: Long,
    val telegram: Long?,
    val gitlabProjectId: Int?,
    val releasesTelegram: Long?)
