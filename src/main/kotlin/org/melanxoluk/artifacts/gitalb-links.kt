package org.melanxoluk.artifacts

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.*
import io.ktor.client.request.forms.submitForm
import io.ktor.http.Parameters


data class Link(
    val id: Int = 0,
    val name: String = "",
    val url: String = "",
    val external: Boolean = true)

class GitlabLinks(private val gitlab: Gitlab): AutoCloseable {
    private val api = "${gitlab.host}/api/v4"

    private val client = HttpClient(OkHttp) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }


    suspend fun getLinks(projectId: Int, tagName: String): List<Link> {
        val url = "$api/projects/$projectId/releases/$tagName/assets/links"
        return client.get(url) {
            header("PRIVATE-TOKEN", gitlab.accessToken)
        }
    }

    suspend fun createLink(projectId: Int, tagName: String, name: String, linkUrl: String): Link  {
        val url = "$api/projects/$projectId/releases/$tagName/assets/links"

        val params = Parameters.build {
            append("name", name)
            append("url", linkUrl)
        }

        return client.submitForm(
            url = url,
            formParameters = params,
            encodeInQuery = false) {
            header("PRIVATE-TOKEN", gitlab.accessToken)
        }
    }

    suspend fun deleteLink(projectId: Int, tagName: String, linkId: Int): Link {
        val url = "$api/projects/$projectId/releases/$tagName/assets/links/$linkId"
        return client.delete(url) {
            header("PRIVATE-TOKEN", gitlab.accessToken)
        }
    }

    override fun close() {
        client.close()
    }
}
