package org.melanxoluk.artifacts

import com.fasterxml.jackson.databind.SerializationFeature
import com.typesafe.config.ConfigFactory
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.*
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.*
import io.ktor.server.netty.Netty
import kotlinx.coroutines.runBlocking
import org.melanxoluk.artifacts.apps.Apps
import org.melanxoluk.artifacts.routes.*
import org.melanxoluk.artifacts.services.GitlabReleases
import java.io.File


object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val config = ConfigFactory.parseFile(File("artifacts.conf"))
        val changelogsDir = config.getString("changelogs-directory")
        val changelogs = Changelogs(changelogsDir)

        val baseDir = config.getString("base-directory")
        val baseUrl = config.getString("base-url")
        val releases = Releases(baseDir, baseUrl, changelogs)
        val apps = Apps(releases)

        val infraAuth = config.getString("infra")
        val infraService = InfraService(infraAuth)

        val gitlabReleases = GitlabReleases(infraService, releases)

        embeddedServer(Netty, applicationEngineEnvironment {
            connector {
                host = "0.0.0.0"
                port = 14000
            }

            module {
                install(AutoHeadResponse)
                install(ContentNegotiation) {
                    jackson {
                        enable(SerializationFeature.INDENT_OUTPUT)
                    }
                }

                routing {
                    static {
                        staticBasePackage = "org.melanxoluk.artifacts"
                        defaultResource("index.html")
                    }

                    // health check
                    get("/ping") { call.respond("pong") }

                    // reload configuration
                    get("/reload") {
                        infraService.reload()
                        call.respond(HttpStatusCode.OK)
                    }

                    get("/projects") { call.respond(infraService.getInfra().projects.values) }

                    // download artifacts
                    artifacts(releases)
                    releases(releases)
                    changelogs(changelogs)
                    apps(apps)

                    gitlabRelease(gitlabReleases)
                    webhooks(gitlabReleases, infraService, releases)

                    // todo: update release
                }
            }
        }).start()

        runBlocking {
            infraService.getInfra().telegrams.values
                .firstOrNull { it.name == "melanxoluk" }
                ?.let { tgSendMessage(it, "artifacts started ") }
        }
    }
}
