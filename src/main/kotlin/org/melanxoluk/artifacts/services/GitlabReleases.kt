package org.melanxoluk.artifacts.services

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import org.gitlab4j.api.GitLabApi
import org.melanxoluk.artifacts.*
import org.slf4j.LoggerFactory


class GitlabReleases(
    private val infraService: InfraService,
    private val releases: Releases) {
    private val log = LoggerFactory.getLogger(GitlabReleases::class.java)

    suspend fun makeRelease(app: String, ver: Version): Release? {
        val infra = infraService.getInfra()
        val project = infra.findProject(app)
        if (project == null) {
            log.error("project not found $project")
            return null
        }

        if (project.gitlabProjectId == null) {
            log.error("project gitlab id null $project")
            return null
        }

        val targetRelease = releases.getRelease(app, ver)
        if (targetRelease == null) {
            log.error("not found release $app $ver")
            return null
        }

        if (targetRelease.changeNotes.isEmpty()) {
            log.error("empty change notes $app $ver")
            return null
        }

        val gitlab = infra.gitlabs.getValue(project.gitlab)
        val api = GitLabApi(gitlab.host, gitlab.accessToken)

        val tag =
            api.tagsApi.createTag(
                project.gitlabProjectId,
                ver.toString(),
                "master")
        log.info("tag created $tag")

        val release =
            api.tagsApi.createRelease(
                project.gitlabProjectId,
                tag.name,
                targetRelease.changeNotes)
        log.info("release created $release")

        GitlabLinks(gitlab).use { links ->
            for (artifact in targetRelease.artifacts()) {
                links.createLink(
                    project.gitlabProjectId,
                    tag.name,
                    artifact.filename,
                    artifact.link)
            }
        }

        return targetRelease
    }

    suspend fun deleteRelease(app: String, ver: Version): Boolean {
        val infra = infraService.getInfra()

        val project = infra.findProject(app)
        if (project == null) {
            log.error("project not found $project")
            return false
        }

        if (project.gitlabProjectId == null) {
            log.info("project gitlab id null $project")
            return false
        }

        val gitlab = infra.gitlabs.getValue(project.gitlab)
        val api = GitLabApi(gitlab.host, gitlab.accessToken)
        api.tagsApi.deleteTag(project.gitlabProjectId, ver.toString())

        return true
    }
}