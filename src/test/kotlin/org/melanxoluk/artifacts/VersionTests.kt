package org.melanxoluk.artifacts

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec


class VersionTests: StringSpec({
    "version should parses successfully" {
        Version.parse("1.1.1") shouldBe Version(1, 1, 1)
        Version.parse("1.1") shouldBe Version(1, 1, 0)
        Version.parse("1") shouldBe Version(1, 0, 0)
        Version.parse("1.1.1.any") shouldBe Version(1, 1, 1, "any")
    }

    "versions should be comparable corrected" {
        (Version(0, 0, 1) == Version(0, 0, 1)) shouldBe true
        (Version(0, 0, 1) < Version(0, 0, 2)) shouldBe true
        (Version(0, 0, 1) < Version(0, 2, 0)) shouldBe true
        (Version(0, 0, 1) < Version(1, 0, 0)) shouldBe true
        (Version(1, 1, 0) < Version(1, 2, 0)) shouldBe true
    }

    "version should updates correctly" {
        VersionUpdate.MAJOR(Version(1, 1, 1)) shouldBe Version(2, 0, 0)
        VersionUpdate.MINOR(Version(1, 1, 1)) shouldBe Version(1, 2, 0)
        VersionUpdate.PATCH(Version(1, 1, 1)) shouldBe Version(1, 1, 2)
    }
})