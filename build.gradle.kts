import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.41"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "org.melanxoluk"
version = "0.1"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("io.ktor:ktor-server-netty:1.2.4")
    implementation("io.ktor:ktor-jackson:1.2.4")
    implementation("io.ktor:ktor-client-okhttp:1.2.4")
    implementation("io.ktor:ktor-client-jackson:1.2.4")

    implementation("com.github.pengrad:java-telegram-bot-api:4.4.0")
    implementation("org.gitlab4j:gitlab4j-api:4.12.10")

    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.apiVersion = "1.3"
    kotlinOptions.languageVersion = "1.3"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "org.melanxoluk.artifacts.Main"
    }

    archiveBaseName.set("artifacts")
    archiveClassifier.set("")
    archiveVersion.set("")
}
